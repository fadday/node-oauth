/*jslint unparam: true*/

var router = require('express').Router();
var accountService = require('../services').accountService;

module.exports = function(app) {
    router.get('/', function(req, res, next) {
        res.send('Hello world');
    });

    router.all('/check', app.oauth.authorise(), function (req, res) {
        res.send('done');
    });

    router.all('/oauth/token', app.oauth.grant());

    router.get('/oauth/authorise', function(req, res, next) {
        if(!req.session.user) {
            return res.redirect('/login?redirect='+req.path+'&client_id='+req.query.client_id+'redirect_uri='+req.query.redirect_uri);
        }

        res.render('authorise', {
            client_id: req.query.client_id,
            redirect_uri: req.query.redirect_uri
        });
    });

    router.post('/oauth/authorise', function(req, res, next) {
        if(!req.session.user) {
            return res.redirect('/login?client_id='+req.query.client_id+'&redirect_uri='+req.query.redirect_uri);
        }

        next();
    }, app.oauth.authCodeGrant(function(req, next) {
        next(null, req.body.allow === 'yes', req.session.user.id, req.session.user);
    }));

    router.get('/login', function(req, res, next) {
        res.render('login');
    });

    router.post('/login', function(req, res, next) {
        accountService.signIn(req.body.username, req.body.password, function(error, result) {
            if(!result) {
                res.render('login', {
                    redirect: req.body.redirect,
                    client_id: req.body.client_id,
                    redirect_uri: req.body.redirect_uri
                });
            } else {
                req.session.user = {
                    id: req.body.username
                };

                return res.redirect((req.body.redirect || '/home')+'?client_id='+req.body.client_id+'&redirect_uri='+req.body.redirect_uri);
            }
        });
    });

    return router;
};
