'use strict'; 

module.exports = function(redisClient) {
    var oauth2Model = {
        getAccessToken: function (accessToken, callback) {
            return redisClient.get("accessToken:" + accessToken, function (error, result){

                if (error !== null) {
                    return callback(error, null);
                }

                if (result !== null) {
                    result = JSON.parse(result);
                    result.access_token = accessToken;

                    return callback(null, result);
                }

                callback(null, null);
            });
        },
        
        saveAccessToken: function (accessToken, clientId, expires, userId, callback) {
            var token = {
                client_id: clientId,
                user_id: userId,
                expires: expires
            };

            var key = 'accessToken:' + accessToken;
            var jsonToken = JSON.stringify(token);

            return redisClient.set(key, jsonToken, function(error){
                callback(error);
            });
        },

        getClient: function (clientId, clientSecret, callback) {
            var key = 'oauthClient:' + clientId;

            return redisClient.get(key, function(error, result) {
                var client = JSON.parse(result);

                if (error !== null) {
                    callback(error, null);
                }

                if(clientSecret !== null) {
                    if(client.client_secret === clientSecret) {
                        callback(null, {
                            clientId: clientId,
                            redirectUri: client.redirect_uri
                        });
                    } else {
                        callback(null, null);
                    }
                } else {
                    if(client !== null) {
                        callback(null, {
                            clientId: clientId,
                            redirectUri: client.redirect_uri
                        });
                    }
                }
            });
        },

        grantTypeAllowed: function (clientId, grantType, callback) {
            var key = 'oauthGrantType:' + clientId;

            return redisClient.sismember(key, grantType, function(error, result) {
                if (error !== null) {
                    return callback(error, null);
                }

                if (result === 1) {
                    return callback(null, true);
                }

                return callback(null, false);
            });
        },

        getUser: function (username, password, callback) {
            var key = 'accounts:username_account:' + username;

            return redisClient.get(key, function (error, accountId) {
                redisClient.get('accounts:info:' + accountId, function (error, result) {
                    if (error !== null) {
                        return callback(error, null);
                    }

                    if (result !== null) {
                        var user = JSON.parse(result);

                        if (user.password === password) {
                            return callback(null, {id: username});
                        }

                        callback(new Error('invalid_grant'), null);

                    } else {
                        return callback(new Error('invalid_grant'), null);
                    }
                });
            });
        },
        
        saveAuthCode: function (authCode, clientId, expires, user, callback) {
            var key = 'oauthAuthCode:' + authCode;

            return redisClient.set(key, JSON.stringify({
                client_id: clientId,
                expires: expires,
                user_id: user
            }), function(error) {
                callback(error);
            });
        },

        getAuthCode: function (authCode, callback) {
            var key = 'oauthAuthCode:' + authCode;

            return redisClient.get(key, function(error, result) {
                if (error !== null) {
                    return callback(error, null);
                }

                if (result !== null) {
                    var code = JSON.parse(result);

                    return callback(null, {
                        authCode: authCode,
                        clientId: code.client_id,
                        expires: code.expires,
                        userId: code.user_id
                    });
                }
                    
                return callback(null, null);
                
            });
        }
    };

    return oauth2Model;
};
