'use strict';

var redis = require('redis');
var client = redis.createClient();

client.on('error', function(error) {
    console.log('Error: ' + error);
});

module.exports = {
    oauth2Model: require('./oauth2Model.js')(client)
};
