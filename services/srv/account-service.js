'use strict';

module.exports = function(redisClient) {
    var accountService = {
        signIn: function(username, password, callback) {
            var accountIdKey = 'accounts:username_account:' + username;
            var accountKey = 'accounts:info:';

            return redisClient.get(accountIdKey, function(error, accountId) {
                redisClient.get(accountKey + accountId, function (error, result) {
                    if (error === null && result !== null) {
                        var account = JSON.parse(result);

                        if (account.password === password) {
                            return callback(null, true);
                        }

                        return callback(null, false);
                    }

                    return callback(error, null);
                });
            });
        }
    };

    return accountService;
};
