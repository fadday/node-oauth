'use strict';
var redis = require('redis');
var client = redis.createClient();

module.exports = {
    accountService: require('./srv/account-service.js')(client)
};
