'use strict';

var express = require('express');
var session = require('express-session');

var app = express();

var bodyParser = require('body-parser');
var oauthServer = require('node-oauth2-server');

app.all('/*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,  Content-Type');

    next();
});


app.use(session({
    secret: 'olala567NiceDice'
}));

app.use(bodyParser());
app.set('view engine', 'ejs');

app.oauth = oauthServer({
    model: require('../models').oauth2Model,
    grants: ['password', 'authorization_code'],
    debug: true
});

var router = require('../routes')(app);
app.use(router);

app.use(app.oauth.errorHandler());

module.exports = app;
