
'use strict';

module.exports = function (grunt) {

    grunt.initConfig({
        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                    quiet: false
                },
                src: ['./tests/**/*.js'] 
            }
        },

        watch: {
            files: '**/*.js',
            tasks: ['mochaTest']             
        },

        'node-inspector': {
            custom: {
                options: {
                    'web-port': 1337,
                    'web-host': 'localhost',
                    'debug-port': 5857,
                    'save-live-edit': false,
                    'no-preload': false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-node-inspector');

    grunt.registerTask('default', ['mochaTest', 'nodeInspector']);
};
