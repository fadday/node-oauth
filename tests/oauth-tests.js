/*global describe: false, it: false*/

'use strict';

var assert = require('assert');
var model  = require(process.cwd() + '/models').oauth2Model;

describe('oauth2Model#getAccessToken', function() {
    it("Should exist", function() {
        assert.notEqual(model, undefined);
    });

    it("Should return access token from db", function(done) {

        var accessToken = 'test_token';

        model.getAccessToken(accessToken, function(error, token) {
            assert.equal(error, null);
            assert.equal(token.access_token, accessToken);

            done();
        });
    });
});

describe('oauth2Model#saveAccessToken', function() {
    it('Should save access token into db', function(done) {
        model.saveAccessToken("test_token", "1", new Date(), "1", function(error){
            if (error !== null) {
                console.log(error);
            } else {
                done();
            }
        });
    });
});

describe('oauth2Model#getClient', function() {
    it('Should return existing client', function(done) {
        model.getClient('888', 'test_secret', function(error, client) {
            assert.equal(error, null);
            assert.equal(client.clientId, '888');
            assert.equal(client.redirectUri, 'http://localhost:3000/');

            done();
        });
    });
});

describe('oauth2Model#grantTypeAllowed', function() {
    it('Should return allowed true for password grant type', function(done) {
        model.grantTypeAllowed('888', 'password', function(error, allowed) {
            assert.equal(error, null);
            assert.equal(allowed, true);

            done();
        });
    });

    it('Should return allowed false for not exist grant type', function(done) {
        model.grantTypeAllowed('888', 'not_exist_type', function(error, allowed) {
            assert.equal(error, null);
            assert.equal(allowed, false);

            done();
        });
    });
});

describe('oauth2Model#getUser', function() {
    it('Should return user', function(done) {
        var username = 'test_user';
        var password = 'test_password';

        model.getUser(username, password, function(error, user) {
            assert.equal(error, null);
            assert.equal(user.id, username);

            done();
        });
    });
});

describe('oauth2Model#saveAuthCode', function() {
    it('Should save auth code into db', function(done) {
        var authCode = 'test_code';
        var clientId = '888';
        var expires = new Date();
        var user = {
            id: 'test_user'
        };

        model.saveAuthCode(authCode, clientId, expires, user, function(error) {
            assert.equal(error, null);

            done();
        });
    });
});

describe('oauth2Model#getAuthCode', function() {
    it('Should get auth code', function(done) {
        var authCode = 'test_code';
        
        model.getAuthCode(authCode, function(error, authCode) {
            assert.equal(error, null);
            assert.equal(authCode.clientId, '888');
            assert.notEqual(authCode.expires, undefined);
            assert.equal(authCode.userId.id, 'test_user');

            done();
        });
    });
});
