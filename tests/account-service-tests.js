/*global describe:false, it:false*/

var assert = require('assert');
var accountService = require(process.cwd() + '/services').accountService;

describe('accountService#signIn', function() {
    it('Should return true for correct user', function(done){
        accountService.signIn('user-2772157', 'uPKftpgOc5mc', function(error, result) {
            assert.equal(error, null);
            assert.equal(result, true);

            done();
        });
    });

    it('Should return false for incorrect password', function(done) {
        accountService.signIn('user-2772157', 'incorrect_password', function(error, result){
            assert.equal(error, null);
            assert.equal(result, false);

            done();
        });
    });
});
