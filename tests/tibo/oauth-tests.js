/*global describe: false, it: false*/

'use strict';

var app = require(process.cwd() + '/app');
var request = require('supertest');
var assert = require('assert');

describe('Oauth2 tests', function () {
    it('Should return token', function (done) {
        var req = request.agent(app);

        req.get('/oauth/authorise?client_id=888&redirect_uri=http://localhost:3000/')
        .end(function (error, res) {
            
            assert.equal(error, null);
            assert.notEqual(res, null);
            
            req.post(res.headers.location)
            .send('username=user-2772157')
            .send('password=uPKftpgOc5mc')
            .send('redirect=/oauth/authorise')
            .send('client_id=888')
            .send('redirect_uri=http://localhost:3000/')
            .end(function(error, res) {

                assert.equal(error, null);
                assert.notEqual(res, null);

                req.get(res.headers.location)
                .end(function(error, res) {
                    
                    assert.equal(res.headers.location, undefined);

                    assert.equal(error, null);
                    assert.notEqual(res, null);

                    req.post('/oauth/authorise')
                    .send('allow=yes')
                    .send('response_type=code')
                    .send('client_id=888')
                    .send('redirect_uri=http://localhost:3000/')
                    .end(function(error, res) {

                        assert.equal(error, null);
                        assert.notEqual(res, null);

                        var authCode = res.headers.location.split('?')[1];

                        req.post('/oauth/token')
                        .send('grant_type=authorization_code')
                        .send(authCode)
                        .send('redirect_uri=http://localhost:3000/')
                        .send('client_id=888')
                        .send('client_secret=test_secret')
                        .end(function(error, res) {
                            
                            assert.equal(res.body.token_type, 'bearer');
                            assert.notEqual(res.body.access_token, undefined);
                            assert.equal(error, null);
                            assert.notEqual(res, null);

                            done();
                        });
                    });
                });
            });
        });
    });
});
